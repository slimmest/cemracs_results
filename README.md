# SLIMMEST CEMRACS 2021 results

## Repository composition
This repository contains:
* notebook including the code to generate all the figures of the presentation. All necessary files are in data so you should be able to run it from scratch.

* notebook with a run of all functionalities developed (dFBA, sampling, metamodeling).
    * It has already been ran, but the generated files have not been saved. You can run it from scratch as well, it should run in around 5 minutes.

* data: a folder containing all necessary files to run the notebook (sbml, yaml) as well as many results obtained: plots for each metamodel, all dFBA experiment used for training, etc.
    * Please note that the Gram matrix used for the training is also contained in the repository. The latter weights 160mo.

* Slides from the presentation
Additionally, the slides of the talk given during the closing session of the CEMRACS 2021.
    * If you want to compile these slides, you may need to install some extra fonts, e.g. [Inria Sans](https://fonts.google.com/specimen/Inria+Sans?query=Inria+sans), [Inria Serif](https://fonts.google.com/specimen/Inria+Serif) and Linux libertine fonts.


## Installing the dfba-sampling package

During the SLIMMEST CEMRACS project, a python package has been developed, needed for RKHS metamodeling of metabolic model. It can be installed with the following command

```bash
pip install dfba-sampling --index-url https://gitlab.inria.fr/api/v4/projects/31648/packages/pypi/simple
```

## Installing additional packages for the notebooks

Additional packages are needed to compute the notebooks. They can be installed with

```bash
pip install jupyter numpy pandas matplotlib scikit-learn
```

## Lauching the notebooks

The two notebooks can now be computed:
* walk-through.ipynb is a tutorial that details all the steps needed for RKHS metamodeling, from training database consitution to metamodel computation and metamodel use.
* figures.ipynb is a tutorial that computes the plots included in the final presentation of the project.
