% !TEX TS-program = XeLaTeX
\documentclass[tikz,code, aspectratio=169]{lifewareslides}

\usepackage{mathtools}
\usepackage{varwidth}
\usepackage{xcolor}
\usepackage{cancel}
\usetikzlibrary{positioning,calc, intersections}
\usepackage{changepage}
\usepackage{blkarray}

\usepackage[ruled, vlined]{algorithm2e}
\usepackage{makecell}
\usepackage{bm}
 \usepackage{tcolorbox}

\graphicspath{{./figures/}}

\newcommand{\R}{\mathbb{R}}
\newcommand{\E}{\mathbb{E}}
\makeatletter
\newcommand{\coplus}{\DOTSB\coplus@\slimits@}
\newcommand{\coplus@}{\mathop{\perp{\oplus}}}
\makeatother

\DeclareMathOperator*{\argmax}{argmax}

\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}

\setbeamertemplate{caption}{\raggedright\insertcaption\par}

\def\bm{\boldsymbol{m}}

\DeclareMathOperator*{\argmin}{argmin}

\title{\LARGE{Metamodeling RKHS FBA}}


\author{Simon Labarthe, Clémence Frioux, David Sherman, Thibault Malou, Julien Martinelli}

\date{August 24th, 2021}

\begin{document}

{\usebackgroundtemplate%
{%
    \includegraphics[width=\paperwidth,height=\paperheight]{./figures/background}%
}
\begin{frame}[t]
   \color{white}
   \vspace{2.05cm}
   {\Large
       \begin{center}
    Metamodeling for metabolic models: application to a PDE model of Salmonella infection
    \end{center}
    \vspace{.25cm}

   }
   \begin{columns}
       \begin{column}{.45\textwidth}
   {\large
       \hfill \begin{center}Simon Labarthe, Clémence Frioux, David Sherman, Thibault Malou, Julien Martinelli\end{center}
   }
   \end{column}
   \begin{column}{.6\textwidth}
   \end{column}
   \end{columns}
\end{frame}
}

\begin{frame}[t]
    \frametitle{Biological context: human microbiomes}

    \begin{itemize}
        \item $10^{13}$ bacterias in human host
        \vfill
    \item \textbf{Hundreds of different species}
    \vfill
\item Bacterial metagenome 150 times bigger than human genome
    \vfill
\item From a bacterial genome we can infer the \textbf{metabolic functions} of the organism
    \end{itemize}
    \begin{center}
    \includegraphics[width=.5\linewidth]{microbiome}
\end{center}
\end{frame}

\begin{frame}[t]
    \frametitle{Case study: food poisoning with Salmonella}

    \begin{itemize}
        \item Complex ecological interactions\vfill
        \item Gut microbiome involved in health (commensal bacteria) and disease (salmonella)\vfill
        \item Spatiotemporal mechanisms perturbed by infection
    \end{itemize}


    \begin{columns}
        \begin{column}{.5\linewidth}
        \includegraphics[width=.7\linewidth]{model0.pdf}
        \end{column}
    \begin{column}{.5\linewidth}
    \includegraphics[width=.7\linewidth]{model.pdf}
\end{column}
\end{columns}

\begin{tcolorbox}[colback=darkblue!5,colframe=darkblue!40,title={\mbox{}}]
    \centering Model the infection: track the space/time evolution of bacteria and metabolites
\end{tcolorbox}

\end{frame}


\begin{frame}[t]
    \frametitle{Population dynamics}
    In the gut, metabolites $(m_i)_{0\leq i\leq N_m}$ and bacteria $(b_i)_{0\leq i\leq N_b}$ interact with each other.\vfill
    (compounds, $N_c = N_m + N_b$)\vfill

    A spatialized population dynamics model (for one bacteria):

    \begin{align}
        \label{eq:population-dynamics-b-spa}
        \partial_t b + \mathcal{T}_b (b) &= \nu_b(m) b \\
        \partial_t m_i + \mathcal{T}_m (m) &= \nu_i(m) b
    \end{align}
    where $\mathcal{T}_b$ and $\mathcal{T}_m$ are transport terms depending on fluid dynamics model and chemotactic speed field.\\%:

    A global population dynamic model:
    \begin{align}
        \label{eq:population-dynamics-b}
        \partial_t b &= \nu_b(m) b \\
        \partial_t m_i &= \nu_i(m) b
    \end{align}
    $\nu_l$: mass flux of compound $l$ induced by the chemical reaction to produce the bacteria $b$, source term of both models.
\end{frame}

\begin{frame}[t]
    \frametitle{Flux Balance Analysis (FBA)}
    \vspace{-.75cm}
    \begin{columns}
        \begin{column}{.6\linewidth}
            \begin{center}
    \includegraphics[width=.45\linewidth]{network.pdf}
\end{center}
        \end{column}
        \begin{column}{.4\linewidth}
            \begin{center}
\[
\begin{blockarray}{cccccc}
r_1 & r_2 & r_3 & r_4 & r_5 \\
\begin{block}{(ccccc)c}
  +1 & 0 & -1 & 0 & 0 & A \\
  0 & +1 & -1 & 0 & 0 & B \\
  0 & 0 & +1 & -1 & 0 & C \\
  0 & 0 & 0 & +1 & -1 & D \\
\end{block}
\end{blockarray}
 \]
 \end{center}
        \end{column}
    \end{columns}
\vspace{.2cm}
    \begin{columns}
        \begin{column}{.3\linewidth}
            Objective
        \end{column}
        \begin{column}{.4\linewidth}
            Steady state assumption $S\nu =0$
        \end{column}
        \begin{column}{.3\linewidth}
            Maximazing $\nu_4$ yields
        \end{column}
    \end{columns}
    \vspace{-.6cm}
        \begin{columns}
            \begin{column}{.3\linewidth}
            $\nu^* = \underset{\substack{S\nu = 0 \\ 0 = c_{\min} \leqslant \nu \leqslant c_{\max} =2}}{\argmax} \nu_4$
            
            \vspace{.75cm}
            (can maximize any function of the form $\beta^T\nu$)
        \end{column}
    \begin{column}{.4\linewidth}

\begin{align*}
    \frac{dA}{dt} &= 0 \Longleftrightarrow \nu_1 - \nu_3 = 0\\
    \frac{dB}{dt} &= 0 \Longleftrightarrow \nu_2 - \nu_3 = 0\\
    \frac{dC}{dt} &= 0 \Longleftrightarrow \nu_3 - \nu_4 = 0\\
    \frac{dD}{dt} &= 0 \Longleftrightarrow \nu_4 - \nu_5 = 0
\end{align*}
\end{column}
\begin{column}{.3\linewidth}

    $\nu = \begin{pmatrix} \nu_1 \\ \nu_2 \\ \nu_3 \\ \nu_4 \\ \nu_5 \end{pmatrix}= \begin{pmatrix} 2 \\ 2 \\ 2 \\ 2 \\ 2 \end{pmatrix}$
\end{column}
\end{columns}

\end{frame}
\begin{frame}[c]
    \frametitle{Example - global population dynamic model}
    \begin{center}
    \includegraphics[width=1\linewidth]{dFBA_example.pdf}
\end{center}
\end{frame}

\begin{frame}[t]
    \frametitle{Metamodeling: approximating FBA}
    From now on: $[c_{\min},c_{\max}]=[c,0]$
    \vfill
    $\nu = (\nu_1, \dots, \nu_{N_c})^T = (\mathcal{F}_{FBA}(c)_1, \dots, \mathcal{F}_{FBA}(c)_{N_c})^T$
    \vspace{.3cm}
\begin{tcolorbox}[colback=darkblue!5,colframe=darkblue!40,title={\mbox{}}]
    \centering $\nu = \mathcal{F}_{FBA}(c)$ is expensive to compute: intractable numerical PDE solve.\\
    \centering Build a metamodel to approximate $c \mapsto \nu_i$ for each compound
\end{tcolorbox}
    \vspace{.3cm}

We also want to investigate constraint interactions in the estimation of $\nu$.

\vfill
Dataset: $(C, \mathcal{V})$ with $C = (c^{j}_i)_{\substack{1\leqslant j \leqslant N_{obs}\\ 1\leqslant i\leqslant N_m}}$ and for a given compound $\mathcal{V} = (\nu^j)_{1\leqslant j\leqslant N_{obs}}$.

\end{frame}

\begin{frame}[t]
    \frametitle{Hoeffding decomposition}
    
    Let $X= (X_1, \dots, X_d)$ be independent variables with law $P_X$ and $h: \mathcal{X} \to \R$ s.t. $h(X) \in L^2(P_X)$.

    \vspace{.3cm}

    \begin{theorem}
        $\lbrack$Hoeffding, 1948$\rbrack$\\

        $\exists$! expansion of $h$ of the form

        \begin{equation}
        h(X)=h_0+\sum_{p\in \mathcal{P}}h_p(X_p)
    \end{equation}
    with:
    \begin{align*}
        h_0 &= \E\lbrack h(X)\rbrack\\
        h_p(X_p) &= \E\lbrack h(X)|X_p\rbrack - \sum_{w\subsetneq p}h_w(X_w) \qquad \forall p \in \mathcal{P}\\
    \end{align*}

    All the $h_p$ are centered and orthogonal with respect of $L^2(\mathcal{X},P_X)$.
\end{theorem}
\vfill
\begin{center}\textbf{From which space should the estimator be picked?}\end{center}

\end{frame}

\begin{frame}[t]
    \frametitle{Reproducing Kernel Hilbert Space (RKHS)}
    Let $\mathcal{H}$ be a Hilbert space of functions $f:\mathcal{X}\subset \R^d\rightarrow \R$ with the inner product $\langle\cdot,\cdot\rangle$.\\ 
    $\mathcal{H}$ is a RKHS if $\forall x\in \mathcal{X}$ the functional
    \begin{align*} 
        L_x : \quad \mathcal{H} &\longrightarrow \R \\
                    f           &\mapsto         f(x)
    \end{align*}
    is continuous.
    \vfill

    From the Riesz theorem, $\exists!~ k_x\in\mathcal{H}$ such that $\forall x\in \mathcal{X},\forall f\in\mathcal{H},f(x)=L_x(f)=\langle f,k_x\rangle$.\\ 
    The reproducing kernel of $\mathcal{H}$ is
    \begin{align*} 
        k : \quad \mathcal{X}\times \mathcal{X} &\longrightarrow \R \\
        (x,x')     &\mapsto         k_{x'}(x)=\langle k_x,k_{x'}\rangle
    \end{align*}
    \end{frame}

    \begin{frame}
        \frametitle{Representer theorem}
    Let $k$ be a positive definite kernel with corresponding RKHS $\mathcal{H}$.\vfill
    Let $\mathcal{G} : \R \to \R$ be a stricly increasing function.\vfill
    Given training points $(x^1, \dots, x^n)$ in $\mathcal{X}$, consider the cost function $J(f) = L(f(x^1), \dots,f(x^n)) + \mathcal{G}(||f||^2_{\mathcal{H}})$
    \vspace{.5cm}
        \begin{theorem}
            $\lbrack$Kimeldorf \& Wahba, 1970$\rbrack$

            If $\hat{f}$ is a function such that $J(\hat{f}) = \inf_{f\in \mathcal{H}} J(f)$, then $f$ admits a representation of the form
    \begin{equation}
        \hat{f}(x)=\sum_{j=1}^{\textcolor{red}{N_{obs}}}\alpha^j k(x^j, x)
    \end{equation}
    with $\alpha^j \in \R~ \forall j$.
    \end{theorem}

    \begin{center}Linear combination of $k(x^j, \cdot) \implies$ parametric optimization problem\end{center}
    \vfill
    In practice, $L = \sum_{j=1}^{N_{obs}} (y^j - \hat{f}(x^j))^2$.

    In the case where $\mathcal{G}(||f||_{\mathcal{H}}) = \lambda ||f||^2_{\mathcal{H}}$ and $L$ is convex, the solution exists and is unique.
\end{frame}

\begin{frame}[t]
    \frametitle{Approximation of the Hoeffding decomposition in a RKHS}
    \framesubtitle{(Durrande \emph{et al.}, 2013), (Huet \& Taupin, 2017)}

    Construction of $\mathcal{H}$ from direct sums of given $\mathcal{H}_a$:
    \begin{align*}
        \mathcal{H}_{0a} &= \lbrace f_a\in\mathcal{H}_a,~ \E_{X_a}\lbrack f_a(X_a)\rbrack = 0\rbrace ~  \forall a\in\{1,\ldots,d\}\\
        k_{0a}(X_a,X'_a) &= k_{a}(X_a,X'_a)-\frac{\E_{U\sim P_a}\lbrack k_a(X_a,U)\rbrack\E_{U\sim P_a}\lbrack k_a(X'_a,U)\rbrack}{\E_{(U,V)\sim P_a\otimes P_a}\lbrack k_a(U,V)\rbrack}
    \end{align*}

    \begin{equation}
        k(X,X') = \left(\prod_{a=1}^d (1 + k_{0a}(X_a,X'_a)\right) = 1 + \sum_{p\in \mathcal{P}} k_p(X_p, X'_p)
    \end{equation}

    with $k_p(X_p, X'_p) = \prod_{a \in p} k_{0a}(X_a, X'_a)$ and $\mathcal{H}_p$ its associated RKHS. Then,

    \begin{equation}
        \mathcal{H} = \left(\prod_{a=1}^d \mathbb{1} \stackrel{\perp}{\oplus} \mathcal{H}_{0a}\right) = \mathbb{1} + \sum_{p \in \mathcal{P}} \mathcal{H}_p
    \end{equation}
    \vfill
    Then, the approximation $\hat{f}$ of $h$ also admits a Hoeffding decomposition, each $\hat{f}_p$ approximates $h_p$.
\end{frame}

\begin{frame}[t]
    \frametitle{Metamodel optimization problem}
    From the representer theorem and the previously constructed RKHS
    \begin{equation} \label{eq:RKHS}
        \hat{\theta}_{0},(\hat{\theta}_{p})_{p\in\mathcal{P}}:= \argmin_{\begin{array}{c}\theta_{0}\in\R \\ \theta_{p} = (\theta_p^j)_{1\leqslant j \leqslant N_{obs}} \forall p\in\mathcal{P}\end{array}} \|\mathcal{V}-(\theta_{0}\mathbb{1}^T + \sum_{p\in\mathcal{P}} K_p \theta_{p}) \|_2^2 + \mathcal{G}(W,\theta_{p})
    \end{equation}
    with $K_p\in\R^{N_{obs}\times N_{obs}}$ the Gram matrix such that $(K_{p~{j_1},{j_2}})_{1\leq {j_1},{j_2}\leq N_{obs}}=k_p(c^{j_1},c^{j_2})$.\vfill

    $\mathcal{G}$ is a regularization term. In this context, the regularization term is a group LASSO term:

\[\mathcal{G}(W,\theta_{p})=N_{obs}\mu\sum_{p\in\mathcal{P}}\|W\theta_p\|_2\]

    with $\mu$ an hyperparameter and $W$ some weight matrix. Ideally, $W=K_p^{1/2}$. For now, $W=Id$.
\end{frame}

\begin{frame}[c]
    \frametitle{L1 penaly enforces sparsity}
    \framesubtitle{(Figure from \emph{An introduction to statistical learning}, Tibshirani \emph{et al.})}
    \begin{center}
    \includegraphics[width=.88\linewidth]{lasso_tshi.pdf}
\end{center}
\end{frame}

\begin{frame}[t]
    \frametitle{Resolution}
    Let $J(\theta_0, \theta) = ||\mathcal{V} - \theta_0\mathbb{1}^T - \sum_{p\in \mathcal{P}} K_p \theta_p||^2 + \sum_{p \in \mathcal{P}} ||W \theta_p||_2$
    \vfill

    \begin{small}
    \begin{algorithm}[H]
    \SetKwBlock{Repeat}{repeat}{}
    Set $\theta = \lbrack 0 \rbrack_{|\mathcal{P}| \times N_{obs}}$\\
\Repeat{
    Compute $\theta_0 = \argmin_{\theta_0}~ J(\theta_0, \theta) = \frac{1}{N_{obs}} \sum_{j=1}^{N_{obs}} \left(\nu^j - \sum_{p \in \mathcal{P}} (K_p \theta_p)_j\right)$\\
    \For{$p \in \mathcal{P}$}{
        Compute $R_p = \mathcal{V} - \theta_0\mathbb{1}^T - \sum_{p \neq w} K_w \theta_w$\\
        \If{$||\frac{2}{\sqrt{N_{obs}}}W^{1/2}R_p|| \leqslant \mu$}{
            $\theta_p \gets 0$ \tcp*{Soft thresholding}
        }
        \Else{
            $\theta_p \gets \argmin_{\theta_p} ~J(\theta_0, \theta)$ \tcp*{one step of gradient descent}
        }
    }
}
\textbf{until} convergence.
\caption{RKHS group lasso}
\end{algorithm}
\end{small}
\end{frame}

\begin{frame}[t]
    \frametitle{Prediction}
    Once the metamodel is trained, we can use it to predict the flux on an unseen constraint sample $c^{new}$:
    \begin{equation}
        \mathcal{F}_{MM}(c^{new}) = \theta_0 + \sum_{j=1}^{\textcolor{red}{N_{obs}}} \sum_{\textcolor{red}{p \in \mathcal{P}}} \theta^j_p k_p(c^j, c^{new})
    \end{equation}
   \pause 
    \vfill
    \begin{columns}
        \begin{column}{.5\linewidth}

Dataset generation
\vspace{.2cm}
    \begin{enumerate}
        \item Simulate multiple ODE trajectories with random initial conditions $y_0$
        \item Subsample the obtained couples $(c, \nu)$ and remove duplicates.
        \item Apply perturbation $c^{pert} = c\varepsilon,~\varepsilon \sim \mathcal{N}(1, \sigma)$ and compute $\nu^{pert} = \mathcal{F}_{FBA}(c^{pert})$
    \end{enumerate}
        \end{column}
\pause
    \begin{column}{.5\linewidth}
        \begin{center}
        We use as kernel the Matern kernel for $k_a$
        \includegraphics[width=1\linewidth]{matern.pdf}
    \end{center}
    \end{column}
\end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{Optimization results ($N_{obs} = 2100, \mu = 0$)}
    \includegraphics[width=1\linewidth]{loss.pdf}
    \[
        \text{Loss:    }   \frac{||\hat{\nu}_i-\nu_i||_2}{||\nu_i||_2}
    \]
\end{frame}

\begin{frame}[t]
    \frametitle{QQ plots on 300 unseen (constraints, fluxes) couples}
    \begin{overprint}
    \onslide<1>\includegraphics[width=1\linewidth]{qqplot_styphi.pdf}
    \onslide<2>\includegraphics[width=1\linewidth]{qqplot_fprau.pdf}
    \end{overprint}
\end{frame}

\begin{frame}[t]
    \frametitle{Simulated trajectory - perfect approximation}
    \includegraphics[width=1\linewidth]{dFBA_perfect.pdf}
\end{frame}

\begin{frame}
    \frametitle{Simulated trajectory - poor approximation}
    \includegraphics[width=1\linewidth]{dFBA_notperfect.pdf}
\end{frame}

\begin{frame}
    \frametitle{Simulated trajectories - $y_{galactose}(0) = 0$}
    \includegraphics[width=1\linewidth]{dFBA_galactose.pdf}
\end{frame}

\begin{frame}
    \frametitle{Simulated trajectory - increased time horizon}
    \includegraphics[width=1\linewidth]{dFBA_long.pdf}
\end{frame}

\begin{frame}[c]
    \frametitle{Lasso paths}
    \begin{center}
    \begin{overprint}
    \onslide<1>\includegraphics[width=.97\linewidth]{lasso_path_styphi.pdf}
    \onslide<2>\includegraphics[width=.97\linewidth]{lasso_path_fprau.pdf}
    \end{overprint}
\end{center}
\end{frame}

\begin{frame}[c]
    \frametitle{Speedup: ODE using FBA versus ODE using metamodel}
    \includegraphics[width=1\linewidth]{speedup_dFBA.pdf}
\end{frame}

\begin{frame}[t]
    \frametitle{Conclusion}
    \begin{itemize}
        \item Python library for:

            \begin{itemize}
                \item Generating and simulating different population dynamics experiments
                \item Learning metamodel of FBA
            \end{itemize}
        \vfill
        \pause
        \item Metamodel build to approximate the Hoeffding decomposition
            \begin{itemize}
                \item Enables fast computations
                \item Allows to investigate the crucial interactions $p \in \mathcal{P}$ of a model.
            \end{itemize}
        \vfill
        \pause
        \item First results convincing, but some dynamics are not well caught
            \begin{itemize}
                \item Lack of goodness of fit when fluxes are $0$ can be corrected.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[c]
    \frametitle{Perspectives}
    \begin{itemize}
        \item Exploring solutions to preserve null fluxes (e.g. another kernel?)
            \vfill

        \item Test different regularization terms
            \vfill
        \item Regularization on observations rather than on subsets
            \vfill
        \item Try other optimization methods
            \vfill
        \item Integration of the results into the PDE model
    \end{itemize}
\end{frame}

\begin{frame}
\frametitle{PDE Simulation}
\begin{center}
\includegraphics[width=0.45\textwidth]{output_PDE_Zavr.pdf}
\end{center}
\begin{small}
\begin{itemize}
\item 2D Finite volume resolution of a system of population dynamics equation with a fluid dynamics model of the gut content (CEMRACS 2015 tribute)
\item $150 \times 20$ space cells $\times \sim 560$ time integration steps $\Rightarrow$ 1 M model evaluations.
\item Longitudinal distribution of radial averaged concentrations.
\item Proof of concept: not physiological $\Rightarrow$ further improvements needed.
\end{itemize}
\end{small}
\end{frame}
\end{document}
