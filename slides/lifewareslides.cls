% Sylvain Soliman
\NeedsTeXFormat{LaTeX2e}[2017/04/15]
\ProvidesClass{lifewareslides}[2019/06/06 v1.2 Pragmatic beamer template for the
Lifeware team trying to abide to the graphical chart]

\RequirePackage{xkeyval}
\DeclareOptionX{aspectratio}[169]{\PassOptionsToClass{\CurrentOption}{beamer}}
\DeclareOptionX{handout}{\PassOptionsToClass{\CurrentOption}{beamer}}
\newif\ifborder\borderfalse%
\DeclareOptionX{border}[none]{%
   \bordertrue%
   \ifnum\strcmp{#1}{blue}=0
      \newcommand{\bordercolor}{inriableuclair}
   \else
      \newcommand{\bordercolor}{inriagrisclair}
   \fi
}
\ProcessOptionsX\relax%

\LoadClass[xelatex,11pt]{beamer}

\newif\ifcode\codefalse%
\DeclareOption{code}{\codetrue}
\newif\ifnag\nagtrue%
\DeclareOption{nag}{\nagfalse}
\newif\ifnotes\notesfalse%
\DeclareOption{notes}{\notestrue}
\newif\iftikz\tikzfalse%
\DeclareOption{tikz}{\tikztrue}
\newif\iffrench\frenchfalse%
\DeclareOption{french}{\frenchtrue}
\newif\ifblock\blockfalse%
\DeclareOption{blocks}{\blocktrue}
\ProcessOptions\relax%

\iffrench\RequirePackage[french]{babel}\fi
\ifnag\RequirePackage{nag}\fi

\RequirePackage[only,llbracket,rrbracket,Arrownot]{stmaryrd}
\RequirePackage[left]{eurosym}
\RequirePackage{graphicx}
\RequirePackage{xspace}
\RequirePackage{microtype}
\RequirePackage[math-style=ISO,bold-style=ISO]{unicode-math}
%\RequirePackage{unicode-math}
\RequirePackage{booktabs}

\RequirePackage{xltxtra}
\defaultfontfeatures{Mapping=tex-text}

\RequirePackage{xcolor}
\definecolor{green}{rgb}{0.0,0.65,0.0}
\definecolor{darkblue}{RGB}{0,0,139}
\definecolor{inriared}{RGB}{230,51,18}       % new new inria
\definecolor{inriablue}{RGB}{56,66,87}       % new new inria
\definecolor{inriarouge}{RGB}{230,51,18}
\definecolor{inriagrisbleu}{RGB}{56,66,87}
\definecolor{inriaorange}{RGB}{240,126,38}
\definecolor{inriajaune}{RGB}{255,205,28}
\definecolor{inriavert}{RGB}{149,193,31}
\definecolor{inriamauve}{RGB}{101,97,169}
\definecolor{inrialilas}{RGB}{155,0,79}
\definecolor{inriavertclair}{RGB}{199,214,79}
\definecolor{inriableuclair}{RGB}{137,204,202}
\definecolor{inriableu}{RGB}{20,136,202}
\definecolor{inriagrisclair}{RGB}{230,231,232}

\ifcode%
   \RequirePackage{listings}
   \lstdefinestyle{Python}{%
      language=Python,
      showtabs=true,
      showstringspaces=false,
      tabsize=4,
      morestring=[b][keywordstyle2]{__}
   }
   \lstset{%
      commentstyle=\color{inriavert},
      stringstyle=\color{magenta},
      keywordstyle=\color{inrialilas},
      keywordstyle={[2]\color{inriaorange}},
      emphstyle=\color{inriared},
      language=biocham,
      basicstyle=\ttfamily,
      keepspaces=true,
   }
\fi

\iftikz%
   \RequirePackage{tikz}
   \usetikzlibrary{arrows.meta,calc,petri,positioning,shapes,shadows,graphs}
   %,decorations.pathmorphing,backgrounds,fit}
   \tikzset{%
         every place/.style={draw=inriaorange!70,fill=inriaorange!20,thick,
            minimum size=8mm},
         every transition/.style={draw=inriamauve!50,fill=inriamauve!20,thick,
            minimum size=6mm},
         pre/.style={<-,shorten <=1pt,>=stealth,thick},
         post/.style={->,shorten >=1pt,>=stealth,thick},
         round/.style={rounded corners=5pt},
         fire/.style={transition,fill=yellow},
         posreg/.style={->,shorten >=1pt,>=stealth,very thick,green},
         negreg/.style={-|,shorten >=1pt,>=stealth,very thick,red},
         ampersand replacement=\&,
% https://tex.stackexchange.com/questions/55806/mindmap-tikzpicture-in-beamer-reveal-step-by-step/55849#55849
         invisible/.style={opacity=0},
         visible on/.style={alt={#1{}{invisible}}},
         alt/.code args={<#1>#2#3}{%
            \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
         },
   }
\fi

\ifnotes%
  % % generate notes for each slide
  \makeatletter
  \def\beamer@framenotesbegin{% at beginning of slide
    \gdef\beamer@noteitems{}%
    \gdef\beamer@notes{{}}% used to be totally empty.
  }
  \makeatother
  % \setbeameroption{show only notes}
\fi

\usefonttheme{professionalfonts}
\setmathfont{Asana-Math.otf} % provided by unicode-math
\setsansfont{Inria Sans}[SmallCapsFont={Linux Libertine O}]
\setromanfont{Inria Serif}
\setmonofont{Courier New}

%%% https://tex.stackexchange.com/questions/55664/fake-small-caps-with-xetex-fontspec
\newcommand\fauxsc[1]{\textrm{\fauxschelper#1 \relax\relax}}
\def\fauxschelper#1 #2\relax{%
  \fauxschelphelp#1\relax\relax%
  \if\relax#2\relax\else\ \fauxschelper#2\relax\fi%
}
\def\Hscale{.83}\def\Vscale{.72}\def\Cscale{1.00}
\def\fauxschelphelp#1#2\relax{%
  \ifnum`#1>``\ifnum`#1<`\{\scalebox{\Hscale}[\Vscale]{\uppercase{#1}}\else%
    \scalebox{\Cscale}[1]{#1}\fi\else\scalebox{\Cscale}[1]{#1}\fi%
  \ifx\relax#2\relax\else\fauxschelphelp#2\relax\fi}
%%%

\usetheme{Boadilla}
\setbeamertemplate{footline}{%
   \hfill
   \begin{beamercolorbox}[ht=2.25ex,dp=1ex,right]{}
      \insertframenumber{}
      \hspace*{1ex}
   \end{beamercolorbox}
}
\setbeamertemplate{navigation symbols}{}
\setbeamerfont{frametitle}{size=\large,family=\rmfamily}
\usecolortheme[named=darkblue]{structure}
\setbeamercolor{normal text}{fg=inriablue}

\newcommand{\titleframe}[1]{%
   {%
      \setbeamercolor{background canvas}{bg=inriablue}
      \begin{frame}
         \begin{center}
            \textcolor{white}{\fontsize{40}{40}\selectfont #1}
         \end{center}
      \end{frame}
      \setbeamercolor{background canvas}{bg=white}
   }
}

\newcommand{\pictureframe}[2]{%
   {%
      \usebackgroundtemplate{%
         \includegraphics[height=\paperheight]{#1}
      }
      \begin{frame}[plain]
         #2
      \end{frame}
   }
}

\newcommand{\hpictureframe}[2]{%
   {%
      \usebackgroundtemplate{%
         \includegraphics[width=\paperwidth]{#1}
      }
      \begin{frame}[plain]
         #2
      \end{frame}
   }
}

\renewcommand{\cite}[1]{{\small [#1]}}

\renewcommand\theenumi{\roman{enumi}}

\ifblock%
\else%
   \setbeamertemplate{theorem begin}
   {%
      % \inserttheoremheadfont% uncomment if you want amsthm-like formatting
      {
         \small\textbf{%
            \inserttheoremname%
            % \inserttheoremnumber
         \ifx\inserttheoremaddition\@empty\else: \inserttheoremaddition\fi%
            % \inserttheorempunctuation
         }
      }
      \hskip\thm@headsep%
   }
   \setbeamertemplate{theorem end}{\newline}
   \setbeamertemplate{proof begin}{{\small\textbf{Proof}}\hskip\thm@headsep}
   \setbeamertemplate{proof end}{}
\fi%

\newtheorem{proposition}{Proposition}
\newenvironment{thm}{\begin{theorem}}{\end{theorem}}
\newenvironment{prop}{\begin{proposition}}{\end{proposition}}
\newenvironment{cor}{\begin{corollary}}{\end{corollary}}
\newenvironment{defi}{\begin{definition}}{\end{definition}}
\newenvironment{lem}{\begin{lemma}}{\end{lemma}}

\ifborder%
   \setbeamertemplate{background canvas}{%
      \begin{tikzpicture}
         \draw[fill=\bordercolor,draw=none] (current page.south west) rectangle
            (\paperwidth,\paperheight);
         \draw[fill=inriared,draw=none] (current page.north west) rectangle
            (.5\paperheight,.5\paperheight);
         \draw[fill=white,draw=none] (current page.south west)
            ++(.08\paperheight,.15\paperheight) rectangle
            ({\paperwidth-.08\paperheight},.92\paperheight);
      \end{tikzpicture}
   }
   \addtobeamertemplate{frametitle}{\vspace*{.08\paperheight}%
      \hspace*{.08\paperheight}}{}
   \newlength{\mymargin}
   \setlength{\mymargin}{.08\paperheight}
   \addtolength{\mymargin}{1cm}
   \setbeamersize{text margin left=\mymargin}
   \setbeamersize{text margin right=\mymargin}
   \setbeamercolor{frametitle}{fg=inriablue}
   \setbeamertemplate{footline}{%
      \hbox{
         \begin{beamercolorbox}[%
            wd=.5\paperwidth,dp=0ex,sep=.08\paperheight,left]{}
            \insertframenumber{} --- \insertshortdate{}
         \end{beamercolorbox}%
         \begin{beamercolorbox}[%
            wd=.5\paperwidth,ht=2ex,dp=0ex,sep=.08\paperheight,right]{}
            \includegraphics[height=.07\paperheight,trim= 0 25 0 0]{%
               inr_logo_scientifique_eng_inverse_v2.png}
         \end{beamercolorbox}
      }
   }
\fi

\endinput
